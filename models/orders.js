// grab the things we need
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// add the Currency type to the Mongoose Schema types
require('mongoose-currency').loadType(mongoose);
var Currency = mongoose.Types.Currency;

//create a order details Schema
var orderDetailSchema = new Schema({
	dishId: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Dish',
		required: true
	},
	quantity: {
		type: Number,
		required: true
	},
	SubTotalPrice: {
		type: Currency,
		required: true
	}
}, {
	timestamps: true
});

//create a orders Schema
var orderSchema = new Schema({
	userId: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User',
		required: true
	},
	companyId: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Company',
		required: true
	},
	totalPrice: {
		type: Currency,
		required: true
	},
	details: [orderDetailSchema]
}, {
	timestamps: true
});

// create model
var Orders = mongoose.model('Orders', orderSchema);

// export model
module.exports = Orders;