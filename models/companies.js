// grab the things we need
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//create a comment Schema
var commentSchema = new Schema({
	rating: {
		type: Number,
		min: 1,
		max: 5,
		required: true
	},
	comment: {
		type: String,
		required: true
	},
	postedBy: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User'
	}
}, {
	timestamps: true
});

//create a dish category Schema
var dishCategorySchema = new Schema({
	name: {
		type: String,
		required: true
	}
}, {
	timestamps: true
});

//create a hours Schema
var hoursSchema = new Schema({
	monday: {
		enable: {
			type: Boolean,
			default: true
		},
		start: {
			type: Number,
			default: 0
		},
		end: {
			type: Number,
			default: 0
		}
	},
	tuesday: {
		enable: {
			type: Boolean,
			default: true
		},
		start: {
			type: Number,
			default: 0
		},
		end: {
			type: Number,
			default: 0
		}
	},
	wednesday: {
		enable: {
			type: Boolean,
			default: true
		},
		start: {
			type: Number,
			default: 0
		},
		end: {
			type: Number,
			default: 0
		}
	},
	thursday: {
		enable: {
			type: Boolean,
			default: true
		},
		start: {
			type: Number,
			default: 0
		},
		end: {
			type: Number,
			default: 0
		}
	},
	friday: {
		enable: {
			type: Boolean,
			default: true
		},
		start: {
			type: Number,
			default: 0
		},
		end: {
			type: Number,
			default: 0
		}
	},
	saturday: {
		enable: {
			type: Boolean,
			default: true
		},
		start: {
			type: Number,
			default: 0
		},
		end: {
			type: Number,
			default: 0
		}
	},
	sunday: {
		enable: {
			type: Boolean,
			default: true
		},
		start: {
			type: Number,
			default: 0
		},
		end: {
			type: Number,
			default: 0
		}
	}
}, {
	timestamps: true
});

//create a companies Schema
var companySchema = new Schema({
	userId: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User'
	},
	name: {
		type: String,
		required: true
	},
    categoryId: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'CompanyCategory'
	},
	adres: {
		type: String,
		required: true
	},
	phone: {
		type: String,
		default: ''
	},
	description: {
		type: String,
		default: ''
	},
	image: {
		type: String,
		default: ''
	},
    delivery: {
		type: Boolean,
		default: false
	},
    takeout: {
		type: Boolean,
		default: true
	},
	hours: hoursSchema,
	comments: [commentSchema],
    dishCategories: [dishCategorySchema]
}, {
	timestamps: true
});

// create model
var Companies = mongoose.model('Company', companySchema);

// export model
module.exports = Companies;