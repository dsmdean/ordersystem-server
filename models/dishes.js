// grab the things we need
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// add the Currency type to the Mongoose Schema types
require('mongoose-currency').loadType(mongoose);
var Currency = mongoose.Types.Currency;

//create a dish Schema
var dishSchema = new Schema({
	companyId: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Company'
	},
    name: {
		type: String,
		required: true
	},
	image: {
		type: String,
		required: true
	},
    categoryId: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'DishCategory'
	},
	price: {
		type: Currency,
		required: true
	},
	description: {
		type: String,
		default: ''
	},
	stock: {
		type: Boolean,
		default: true	
	}
}, {
	timestamps: true
});

// create model
var Dishes = mongoose.model('Dish', dishSchema);

// export model
module.exports = Dishes;