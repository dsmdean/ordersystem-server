// grab the things we need
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var passportLocalMongoose = require('passport-local-mongoose');

//create a favorites Schema
var favoritesSchema = new Schema({
	company: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Company',
		required: true
	}
}, {
	timestamps: true
});

//create a users Schema
var User = new Schema({
	username: String,
	password: String,
	OauthId: String,
	OauthToken: String,
	firstname: {
		type: String,
		default: ''
	},
	lastname: {
		type: String,
		default: ''
	},
    email: String,
    favorites: [favoritesSchema],
	admin: {
		type: Boolean,
		default: false
	},
    company: {
		type: Boolean,
		default: false
	}
}, {
	timestamps: true
});

//create a model method
User.methods.getName = function() {
	return (this.firstname + ' ' + this.lastname)
};

// create model
User.plugin(passportLocalMongoose);

// export model
module.exports = mongoose.model('User', User);