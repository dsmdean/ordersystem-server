// grab the things we need
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//create a company category Schema
var companyCategorySchema = new Schema({
	name: {
		type: String,
		required: true
	}
}, {
	timestamps: true
});

// create model
var CompanyCategory = mongoose.model('CompanyCategory', companyCategorySchema);

// export model
module.exports = CompanyCategory;