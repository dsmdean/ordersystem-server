// grab the things we need
var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

// import models
var Orders = require('../models/orders');
var Verify = require('./verify');

// create router
var orderRouter = express.Router();
orderRouter.use(bodyParser.json());

orderRouter.route('/')
// get all orders
.get(function(req, res,next) {
	Orders.find({})
    .populate('userId')
    .populate('details.dishId')
    .exec(function(err, orders) {
        if(err) next(err);
        res.json(orders);
    });
})
// place order
.post(function(req, res,next) {
	Orders.create(req.body, function(err, order) {
        if(err) next(err);
        
        var id = order._id;
        res.writeHead(200, {
            'Content-Type': 'text/plain'
        });
        
        res.end('Added the order with id ' + id);
    });
});

orderRouter.route('/:orderId')
// get a specifi order
.get(function(req, res, next) {
	Orders.findById(req.params.orderId)
    .populate('userId')
    .populate('details.dishId')
    .exec(function(err, order) {
        if(err) next(err);
        res.json(order);
    })
});

// export router
module.exports = orderRouter;