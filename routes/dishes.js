// grab the things we need
var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

// import models
var Dishes = require('../models/dishes');
var Verify = require('./verify');

// create router
var dishRouter = express.Router();
dishRouter.use(bodyParser.json());

dishRouter.route('/')
// get all dishes
.get(function(req, res,next) {
	Dishes.find({}) 
    .populate('companyId')
    .exec(function(err, dishes) {
        if(err) next(err);
        res.json(dishes);
    });
})
// place a dish
.post(function(req, res,next) {
	Dishes.create(req.body, function(err, dish) {
        if(err) next(err);
        
        var id = dish._id;
        res.writeHead(200, {
            'Content-Type': 'text/plain'
        });
        
        res.end('Added the dish with id ' + id);
    });
});

dishRouter.route('/:dishId')
// get a specific dish
.get(function(req, res, next) {
	Dishes.findById(req.params.dishId)
    .populate('companyId')
    .exec(function(err, dish) {
        if(err) next(err);
        res.json(dish);
    })
})
// update a specific dish
.put(function(req, res,next) {
	Dishes.findByIdAndUpdate(req.params.dishId, {
        $set: req.body
    }, {
        new: true
    }, function(err, dish) {
        if(err) next(err);
        res.json(dish);
    });
})
// delete a specific dish
.delete(function(req, res,next) {
	Dishes.findById(req.params.dishId, function(err, dish) {
        if(err) next(err);
        
        dish.remove({});
        res.json(dish);
    });
});

dishRouter.route('/company/:companyId')
// get dishes from a specific company
.get(function(req, res, next) {
	Dishes.find({'companyId': req.params.companyId}, function(err, dishes) {
        if(err) next(err);
        res.json(dishes);
    });
})
// place dish for a specific company
.post(function(req, res,next) {
    req.body.companyId = req.params.companyId;
	Dishes.create(req.body, function(err, dish) {
        if(err) next(err);
        
        var id = dish._id;
        res.writeHead(200, {
            'Content-Type': 'text/plain'
        });
        
        res.end('Added the dish with id '+ id);
    });
});

// export router
module.exports = dishRouter;