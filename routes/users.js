// grab the things we need
var express = require('express');
var userRouter = express.Router();
var passport = require('passport');

// import models
var Orders = require('../models/orders');
var User = require('../models/users');
var Verify = require('./verify');

// get all users
userRouter.get('/', function(req, res, next) {
  User.find({}, function(err, user) {
		if(err) next(err);
		res.json(user);
	});
});

userRouter.route('/:userId')
// get a specific user
.get(function(req, res, next) {
	User.findById(req.params.userId, function(err, user) {
		if(err) next(err);
		res.json(user);
	});
})
// update a specific user
.put(function(req, res,next) {
	User.findByIdAndUpdate(req.params.userId, {
		$set: req.body
	}, {
		new: true
	}, function(err, user) {
		if(err) next(err);
		res.json(user);
	});
})
// delete a specific user
.delete(function(req, res,next) {
	User.findById(req.params.userId, function(err, user) {
		if(err) next(err);
        
    user.remove({});
		res.json(user);
	});
});

userRouter.route('/:userId/favorites')
// get favorites from a specific user
.get(function(req, res, next) {
	User.findById(req.params.userId)
	.populate('favorites.company')
	.exec(function(err, user) {
    if(err) next(err);
    res.json(user.favorites);
  });
})
// place favorite for a specific user
.post(function(req, res,next) {
	User.findById(req.params.userId, function(err, user) {
    if(err) next(err);
    
	user.favorites.push(req.body);

    user.save(function(err, userFavorite) {
      if(err) next(err);
      res.json(userFavorite);
    });
  });
});

userRouter.route('/:userId/favorites/:favoriteId')
// delete a specific favorite from a specific user
.delete(function(req, res,next) {
	User.findById(req.params.userId, function(err, user) {
		if(err) next(err);
        
    user.favorites.id(req.params.favoriteId).remove();
    user.save(function(err, user) {
      if(err) next(err);
      res.json(user);
    });
	});
});

userRouter.route('/:userId/orders')
// get all orders associated with a specific user
.get(function(req, res, next) {
	Orders.find({'userId': req.params.userId})
	.populate('userId')
    .populate('details.dishId')
	.exec(function(err, orders) {
    if(err) next(err);
    res.json(orders);
  });
});

// register user
userRouter.post('/register', function(req, res) {
	User.register(new User({ username: req.body.username }),
		req.body.password, function(err, user) {
			if(err) {
				return res.status(500).json({err: err});
			}

			if(req.body.firstname) {
				user.firstname = req.body.firstname;
			}

			if(req.body.lastname) {
				user.lastname = req.body.lastname;
			}

			user.save(function(err, user) {
				passport.authenticate('local')(req, res, function() {
					return res.status(200).json({status: 'Registration Successful!'});
				});
			});
		});
});

// log user in
userRouter.post('/login', function(req, res, next) {
	passport.authenticate('local', function(err, user, info) {
		if(err) {
			return next(err);
		}
		if(!user) {
			return res.status(401).json({
				err: info
			});
		}
		req.logIn(user, function(err) {
			if(err) {
				return res.status(500).json({
					err: 'Could not log in user'
				});
			}

			// console.log('User in users: ', user);

			var token = Verify.getToken({"username":user.username, "_id":user._id, "admin":user.admin});

			res.status(200).json({
				status: 'Login Successful!',
				succes: true,
				token: token,
				user: user
			});
		});
	})(req, res, next);
});

// log user out
userRouter.get('/logout', function(req, res) {
	req.logout();
	res.status(200).json({
		status: 'Bye!'
	});
});

// log user in using facebook
userRouter.get('/facebook', passport.authenticate('facebook'),
	function(req, res) {
    
  }
);

// register user using facebook
userRouter.get('/facebook/callback', function(req, res, next) {
	passport.authenticate('facebook', function(err, user, info) {
		if(err) {
			return next(err);
		}
		if(!user) {
			return res.status(401).json({
				err: info
			});
		}
		req.logIn(user, function(err) {
			if(err) {
				return res.status(500).json({
					err: 'Could not log in user'
				});
			}

			var token = Verify.getToken(user);

			res.status(200).json({
				status: 'Login succesful!',
				succes: true,
				token: token
			});
		});
	})(req,res,next);
});

// export router
module.exports = userRouter;