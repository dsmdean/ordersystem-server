// grab the things we need
var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var passport = require('passport');

// import models
var Orders = require('../models/orders');
var Dishes = require('../models/dishes');
var User = require('../models/users');
var Companies = require('../models/companies');
var Verify = require('./verify');

// create router
var companiesRouter = express.Router();
companiesRouter.use(bodyParser.json());

companiesRouter.route('/')
// get all companies
.get(function(req, res,next) {
	Companies.find(req.query)
		.populate('userId')
        .populate('categoryId')
        .populate('comments.postedBy')
		.exec(function(err, companies) {
			if(err) next(err);
			res.json(companies);
	});
})
// register a company
.post(function(req, res,next) {
	// User.findById(req.body.userId, function(err, user) {
	// 	if(err) next(err);
        
    //     if(user.company) {
    //         Companies.find({'userId': user}, function(err, company) {
    //             if(err) next(err);
                
    //             if(!company.length) {
    //                 Companies.create(req.body, function(err, company) {
    //                     if(err) next(err);
                        
    //                     console.log('Company created!');
    //                     var id = company._id;
    //                     res.writeHead(200, {
    //                         'Content-Type': 'text/plain'
    //                     });
                        
    //                     res.end('Added the company with id ' + id);
    //                 });
    //             } else {
    //                 // error
    //                 var err = new Error('Conflict! This user already has a company registered!');
    //                 err.status = 409;
    //                 return next(err);
    //             }
    //         });
    //     } else {
    //         // error
    //         var err = new Error('The request could not be completed because of a conflict in the request!');
    //         err.status = 409;
    //         return next(err);
    //     }
	// });
    
    User.register(new User({ username: req.body.username }),
	    req.body.password, function(err, user) {
		    if(err) {
				return res.status(500).json({err: err});
			}

			if(req.body.firstname) {
				user.firstname = req.body.firstname;
			}

			if(req.body.lastname) {
				user.lastname = req.body.lastname;
			}
            
            user.company = true;

			user.save(function(err, user) {
				passport.authenticate('local')(req, res, function() {
					console.log('Registration Completed!');
                    req.body.userId = user._id;
                    
                    Companies.create(req.body, function(err, company) {
                        if(err) next(err);
                        
                        console.log('Company created!');
                        var id = company._id;
                        res.writeHead(200, {
                            'Content-Type': 'text/plain'
                        });
                        
                        res.end('Added the company with id ' + id);
                    });
				});
			});
		});
});

companiesRouter.route('/user/:userId')
// get company associated with a specific user
.get(function(req, res,next) {
	Companies.findOne({'userId': req.params.userId}, function(err, company) {
			if(err) next(err);
			res.json(company);
	});
})

companiesRouter.route('/:companyId')
// get a specific company
.get(function(req, res, next) {
    Companies.findById(req.params.companyId)
		.populate('userId')
        .populate('categoryId')
        .populate('comments.postedBy')
		.exec(function(err, company) {
			if(err) next(err);
			res.json(company);
	});
})
// update a specific company
.put(function(req, res,next) {
	Companies.findByIdAndUpdate(req.params.companyId, {
        $set: req.body
    }, {
        new: true
    }, function(err, company) {
        if(err) next(err);
        res.json(company);
    });
})
// delete a specific company
.delete(function(req, res,next) {
	Companies.findById(req.params.companyId, function(err, company) {
        if(err) next(err);
        
        company.remove({});
        res.json(company);
    });
});

companiesRouter.route('/:companyId/comments')
// get comments from a specific company
.get(function(req, res, next) {
	Companies.findById(req.params.companyId)
        .populate('comments.postedBy')
        .exec(function(err, company) {
            if(err) next(err);
            res.json(company.comments);
    });
})
// place comments for a specific company
.post(function(req, res,next) {
	Companies.findById(req.params.companyId, function(err, company) {
        if(err) next(err);
        
        company.comments.push(req.body);
        company.save(function(err, companyComments) {
            if(err) next(err);
            console.log('Placed Company Comments!');
            
            res.json(companyComments);
        });
    });
});

companiesRouter.route('/:companyId/comments/:commentId')
// get a specific comment from a specific company
.get(function(req, res, next) {
	Companies.findById(req.params.companyId) 
        .populate('comments.postedBy')
        .exec(function(err, company) {
            if(err) next(err);
            res.json(company.comments.id(req.params.commentId));
    });
})
// update a specific comment from a specific company
.put(function(req, res,next) {
	Company.findById(req.params.companyId, function(err, company) {
        if(err) next(err);
        
        company.comments.id(req.params.commentId).remove();
        company.comments.push(req.body);
        
        company.save(function(err, companyComments) {
            if(err) next(err);
            console.log('Updated Company Comment!');
            
            res.json(companyComments);
        });
    });
})
// delete a specific comment from a specific company
.delete(function(req, res,next) {
	Company.findById(req.params.companyId, function(err, company) {
        if(err) next(err);
        
        company.comments.id(req.params.commentId).remove();
        
        company.save(function(err, companyComments) {
            if(err) next(err);
            console.log('Deleted Company Comment!')
            
            res.json(companyComments);
        })
    })
});

companiesRouter.route('/:companyId/dish-categories')
// get dish categories from a specific company
.get(function(req, res,next) {
	Companies.findById(req.params.companyId, function(err, company) {
        if(err) next(err);
        res.json(company.dishCategories);
    });
})
// place a dish category for a specific company
.post(function(req, res,next) {
	Companies.findById(req.params.companyId, function(err, company) {
        if(err) next(err);
        
        company.dishCategories.push(req.body);
        company.save(function(err, companyDishCategories) {
            if(err) next(err);
            
            res.json(companyDishCategories);
        });
    });
});

companiesRouter.route('/:companyId/dish-categories/:dishCategoryId')
// get a specific dish category from a specific company
.get(function(req, res,next) {
	Companies.findById(req.params.companyId, function(err, company) {
        if(err) next(err);
        res.json(company.dishCategories.id(req.params.dishCategoryId));
    });
})
// update a specific dish category from a specific company
.put(function(req, res,next) {
	Companies.findById(req.params.companyId, function(err, company) {
        if(err) next(err);
        company.dishCategories.id(req.params.dishCategoryId).remove();
        
        company.dishCategories.push(req.body);
        company.save(function(err, companyDishCategories) {
            if(err) next(err);
            
            res.json(companyDishCategories);
        });
    });
})
// delete a specific dish category from a specific company
.delete(function(req, res,next) {
	Companies.findById(req.params.companyId, function(err, company) {
        if(err) next(err);
        
        company.dishCategories.id(req.params.dishCategoryId).remove();
        company.save(function(err, companyDishCategories) {
            if(err) next(err);
            
            res.json(companyDishCategories);
        });
    });
});

companiesRouter.route('/:companyId/dishes')
// get all dishes from a specific company
.get(function(req, res,next) {
	Dishes.find({'companyId': req.params.companyId}, function(err, dishes) {
        if(err) next(err);
        res.json(dishes);
    });
})
// place dish for a specific company
.post(function(req, res,next) {
    req.body.companyId = req.params.companyId;
	Dishes.create(req.body, function(err, dish) {
        if(err) next(err);
        
        var id = dish._id;
        res.writeHead(200, {
            'Content-Type': 'text/plain'
        });
        
        res.end('Added the dish with id '+ id);
    });
});

companiesRouter.route('/:companyId/dishes/:dishcategoryId')
// get all dishes from a specific company with a specific dish category
.get(function(req, res,next) {
	Dishes.find({'companyId': req.params.companyId, 'categoryId': req.params.dishcategoryId}, function(err, dishes) {
        if(err) next(err);
        res.json(dishes);
    });
});

companiesRouter.route('/:companyId/orders')
// get all orders associated with a specific company
.get(function(req, res,next) {
	Orders.find({'companyId': req.params.companyId})
    .populate('userId')
    .populate('details.dishId')
    .exec(function(err, orders) {
        if(err) next(err);
        res.json(orders);
    })
});

// export router
module.exports = companiesRouter;