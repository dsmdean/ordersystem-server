// grab the things we need
var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

// import models
var companyCategories = require('../models/companyCategories');
var Verify = require('./verify');

// create router
var companyCatRouter = express.Router();
companyCatRouter.use(bodyParser.json());

companyCatRouter.route('/')
// get all company categories
.get(function(req, res, next) {
	companyCategories.find({}, function(err, category) {
		if(err) next(err);
		res.json(category);
	});
})
// place a company category
.post(function(req, res,next) {
	companyCategories.create(req.body, function(err, category) {
		if(err) next(err);

		console.log('Company category created!');
		var id = category._id;
		res.writeHead(200, {
			'Content-Type': 'text/plain'
		});

		res.end('Added the category with id: ' + id);
	});
});

companyCatRouter.route('/:categoryId')
// get a specific company category
.get(function(req, res, next) {
	companyCategories.findById(req.params.categoryId, function(err, category) {
		if(err) next(err);
		res.json(category);
	});
})
// update a specific company category
.put(function(req, res,next) {
	companyCategories.findByIdAndUpdate(req.params.categoryId, {
		$set: req.body
	}, {
		new: true
	}, function(err, category) {
		if(err) next(err);
		res.json(category);
	});
})
// delete a specific company category
.delete(function(req, res,next) {
	companyCategories.findById(req.params.categoryId, function(err, category) {
		if(err) next(err);
        
        category.remove({});
		res.json(category);
	});
});

// export router
module.exports = companyCatRouter;